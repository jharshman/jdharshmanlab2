package com.example.jharshman.jdharshmanlab2;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jharshman on 10/11/15.
 */
public class MyListAdapter extends BaseExpandableListAdapter implements View.OnClickListener {

    //private Context context;
    private Activity parentActivity;
    private ArrayList<String> makes;
    private HashMap<String, ArrayList<String>> models;

    public MyListAdapter(Activity parentActivity, ArrayList<String> makes, HashMap<String, ArrayList<String>> models) {
        this.parentActivity = parentActivity;
        this.makes = makes;
        this.models = models;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater)this.parentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.expandable_list_layout, null);
        }
        String title = (String)getGroup(groupPosition);
        TextView txtHeader = (TextView)convertView.findViewById(R.id.textView);
        txtHeader.setTypeface(null, Typeface.BOLD);
        txtHeader.setText(title);
        return convertView;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public int getGroupCount() {
        return this.makes.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.models.get(this.makes.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.makes.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.models.get(this.makes.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if(convertView == null) {
            //inflate
            LayoutInflater layoutInflater = (LayoutInflater)this.parentActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.child_list_layout, null);
        }
        final String childText = (String)getChild(groupPosition,childPosition);
        TextView txtview = (TextView)convertView.findViewById(R.id.textView2);
        txtview.setText(childText);

        //register a listener on the trashcan icon
        ImageView img = (ImageView)convertView.findViewById(R.id.imageView);
        img.setOnClickListener(this);
        img.setTag(R.id.group_num, groupPosition);
        img.setTag(R.id.posn_num,childPosition);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public void onClick(View v) {
        int groupPos = (int)v.getTag(R.id.group_num);
        int childPos = (int)v.getTag(R.id.posn_num);
        models.get(makes.get(groupPos)).remove(childPos);
        notifyDataSetChanged();

    }

}
