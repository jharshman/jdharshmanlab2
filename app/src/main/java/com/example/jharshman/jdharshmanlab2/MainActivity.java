package com.example.jharshman.jdharshmanlab2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    AssetManager mAssetManager;
    ExpandableListAdapter mListAdapter;
    ExpandableListView mListView;
    private ArrayList<String> makes = new ArrayList<String>();
    private HashMap<String, ArrayList<String>> models = new HashMap<String, ArrayList<String>>();
    private boolean flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = (ExpandableListView)findViewById(R.id.expandableListView);

        if(savedInstanceState == null) {

            flag = parseFile("vehicles.txt");
            if(!flag) {
                CharSequence text = "Failed to read input";
                Toast toast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
                toast.show();
            } else {
                mListAdapter = new MyListAdapter(this, makes, models);
                mListView.setAdapter(mListAdapter);
            }

        } else {
            makes = (ArrayList<String>)savedInstanceState.getSerializable("makes");
            models = (HashMap<String, ArrayList<String>>)savedInstanceState.getSerializable("models");
            mListAdapter = new MyListAdapter(this, makes, models);
            mListView.setAdapter(mListAdapter);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about) {
            aboutMe();
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean parseFile(String fname) {

        mAssetManager = getResources().getAssets();
        BufferedReader reader = null;

        try {
            reader = new BufferedReader(new InputStreamReader(mAssetManager.open(fname)));

            int i = 0;
            String line;
            while((line = reader.readLine()) != null) {

                ArrayList<String> modelTemp = new ArrayList<String>();
                String[] temp = line.split(",");

                makes.add(temp[0]);
                for(int j = 1; j < temp.length; j++)
                    modelTemp.add(temp[j]);


                models.put(makes.get(i), modelTemp);
                i++;

            }

        }catch(IOException e) {
            return false;
        }finally {
            try {
                if(reader!=null)
                    reader.close();
            }catch(IOException e) {
                e.printStackTrace();
            }
        }

        return true;

    }

    public void onSaveInstanceState(Bundle bundle) {
        //save list
        bundle.putSerializable("models", models);
        bundle.putSerializable("makes", makes);
    }

    public void aboutMe() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.attr_dialog_title)
                .setItems(R.array.about_me, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();
    }


}
